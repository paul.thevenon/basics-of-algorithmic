from random import shuffle
from typing import Any, Callable, List
from copy import deepcopy


def __test_sort_immutable(
    f: Callable[[List[Any]], List[Any]],
    l: List[Any],
    expected: List[Any],
    msg: str,
):
    before = deepcopy(l)
    actual = f(l)
    assert before == l, "sort should not mutate input list"
    assert actual == expected, msg


def __test_sort(f: Callable[[List[Any]], List[Any]], n: int):
    l_to_sort = list(range(n))
    sorted = list(range(n))

    __test_sort_immutable(f, l_to_sort, sorted, "best case should work")
    __test_sort_immutable(f, list(reversed(l_to_sort)), sorted, "worst case should work")

    shuffle(l_to_sort)
    __test_sort_immutable(f, l_to_sort, sorted, "average case should work")


def __test_sort_int(f: Callable[[List[int]], List[int]], n: int):
    l_to_sort = list(range(n))
    sorted = list(range(n))

    shuffle(l_to_sort)
    assert f(l_to_sort, lambda a, b: a - b) == sorted

    shuffle(l_to_sort)
    assert f(l_to_sort, lambda a, b: b - a) == list(reversed(sorted))


def __test_sort_string(f: Callable[[List[str]], List[str]]):
    l_to_sort = ["foo", "brr", "baz", "bar"]
    sorted = ["bar", "baz", "brr", "foo"]

    assert f(l_to_sort, lambda a, b: -1 if a < b else 0 if a == b else 1) == sorted


def __test_sort_dict(f: Callable[[List[dict]], List[dict]]):
    l_to_sort = [
        {"name": "Charlie", "age": 25},
        {"name": "Alice", "age": 26},
        {"name": "Bob", "age": 22},
    ]
    sorted_by_name = [
        {"name": "Alice", "age": 26},
        {"name": "Bob", "age": 22},
        {"name": "Charlie", "age": 25},
    ]
    sorted_by_age = [
        {"name": "Bob", "age": 22},
        {"name": "Charlie", "age": 25},
        {"name": "Alice", "age": 26},
    ]

    assert f(l_to_sort, lambda a, b: a["age"] - b["age"]) == sorted_by_age
    assert f(
        l_to_sort,
        lambda a, b: -1 if a["name"] < b["name"] else 0 if a["name"] == b["name"] else 1,
    ) == sorted_by_name


def test_bubble():
    from src.generic_sort import bubble_sort
    for n in range(20):
        __test_sort(bubble_sort, n)

    __test_sort_int(bubble_sort, 10)
    __test_sort_string(bubble_sort)
    __test_sort_dict(bubble_sort)


def test_selection():
    from src.generic_sort import selection_sort
    for n in range(20):
        __test_sort(selection_sort, n)

    __test_sort_int(selection_sort, 10)
    __test_sort_string(selection_sort)
    __test_sort_dict(selection_sort)


def test_quick_sort():
    from src.generic_sort import quick_sort
    for n in range(20):
        __test_sort(quick_sort, n)

    __test_sort_int(quick_sort, 10)
    __test_sort_string(quick_sort)
    __test_sort_dict(quick_sort)


def test_merge_sort():
    from src.generic_sort import merge_sort
    for n in range(20):
        __test_sort(merge_sort, n)

    __test_sort_int(merge_sort, 10)
    __test_sort_string(merge_sort)
    __test_sort_dict(merge_sort)
