from typing import Callable

EXPECTED = [
    (0, 0),
    (1, 1),
    (2, 1),
    (3, 2),
    (4, 3),
    (5, 5),
    (6, 8),
    (7, 13),
    (8, 21),
    (9, 34),
    (10, 55),
    (11, 89),
    (12, 144),
    (13, 233),
    (14, 377),
    (15, 610),
    (16, 987),
    (17, 1597),
    (18, 2584),
    (19, 4181),
    (20, 6765),
]


def __test_fibo(f: Callable[[int], int]):
    for n, f_n in EXPECTED:
        actual = f(n)
        assert actual == f_n, f"F_{n} should be {f_n}, found {actual}"


def test_fibo_rec():
    from src.fibo import fibo
    __test_fibo(fibo)


def test_fibo_memo():
    from src.fibo import fibo_memo
    __test_fibo(fibo_memo)


def test_fibo_iter():
    from src.fibo import fibo_iter
    __test_fibo(fibo_iter)
