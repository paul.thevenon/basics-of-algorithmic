# Basics of algorithmic

> > In mathematics and computer science, an algorithm (/ˈælɡərɪðəm/) is a
> > finite sequence of mathematically rigorous instructions, typically used to
> > solve a class of specific problems or to perform a computation. Algorithms
> > are used as specifications for performing calculations and data processing.
>
> [Wikipedia page for "_Algorithm_"](https://en.wikipedia.org/wiki/Algorithm)

During this class, you will learn about algorithms and recursion, time and space
complexities and some classic examples.

## Table of content
- [_Upload your work to the LMS_](#upload-your-work-to-the-lms-toc)
- [_Requirements_](#requirements-toc)
- [_Introduction_](#introduction-toc)
- [_The basics with the Fibonacci sequence_](#the-basics-with-the-fibonacci-sequence-toc)
  - [_Question 1_](#q1-toc)
  - [_Question 2_](#q2-toc)
    - [_Memoization_](#memoization-toc)
  - [_Question 3_](#q3-toc)
    - [_Making algorithms iterative_](#making-algorithms-iterative-toc)
  - [_Question 4_](#q4-toc)
  - [_Studying the complexity of our algorithms_](complexity.md#studying-the-complexity-of-our-algorithms-toc)
    - [_The different types of complexities_](complexity.md#the-different-types-of-complexities-toc)
    - [_Number of operations vs asymptotic complexities_](complexity.md#number-of-operations-vs-asymptotic-complexities-toc)
    - [_The $O$, $\Theta$ and $\Omega$ notations_](complexity_notations.md#the-o-theta-and-omega-notations-toc)
    - [_Classic families of algorithms_](complexity_families_and_practice.md#classic-families-of-algorithms-toc)
    - [_How to compute complexities in practice_](complexity_families_and_practice.md#how-to-compute-complexities-in-practice-toc)
    - [_The Master Theorem_](complexity_families_and_practice.md#the-master-theorem-toc)
  - [_Question 5_](complexity_fibonacci.md#q5-toc)
  - [_Question 6_](complexity_fibonacci.md#q6-toc)
  - [_Question 7_](complexity_fibonacci.md#q7-toc)
- [_Sorting algorithms_](sorting.md#sorting-algorithms-toc)
  - [_Bubble sort_](sorting.md#bubble-sort-toc)
    - [_Question 8_](sorting.md#q8-toc)
  - [_Selection sort_](sorting.md#selection-sort-toc)
    - [_Question 9_](sorting.md#q9-toc)
  - [_Quick sort_](sorting.md#quick-sort-toc)
    - [_Question 10_](sorting.md#q10-toc)
  - [_Merge sort_ (_optional_)](sorting.md#merge-sort-toc-optional)
    - [_Question 11_](sorting.md#q11-toc)
  - [_Question 12_](sorting.md#q12-toc)
  - [_Generalizing to any type_](sorting.md#generalizing-to-any-type-toc)
    - [_Question 13_](sorting.md#q13-toc)
    - [_Question 14_](sorting.md#q14-toc)
  - [_Visualization_](sorting.md#visualization-toc)
    - [_Question 15_](sorting.md#q15-toc)
    - [_Question 16_](sorting.md#q16-toc)

![Structures](./assets/110256307-e6347c80-7f76-11eb-9635-df55b02d540a.png)

---

## Requirements [[toc](#table-of-content)]
First, have a look at the [_Recursion_ section of the 5th notebook](https://gitlab.isae-supaero.fr/mae-ac/learn-python/-/blob/main/notebooks/05_advanced.ipynb)

## Upload your work to the LMS [[toc](#table-of-content)]
- open a terminal
- go into the folder containing your project
- use the `zip` command to compress your project
```shell
zip -r project.zip . -x "venv/**" ".git/**"
```
- upload the ZIP archive to the [LMS](https://lms.isae.fr/mod/assign/view.php?id=116611&action=editsubmission)

---

## Introduction [[toc](#table-of-content)]

Most algorithms are a sequence of more or less intricate instructions. The goal
is to transform some data to achieve a given task. That data is stored in _data
structures_, e.g. _lists_, _trees_ or _graphs_.

In this class, we will focus on lists by
- implementing the _Fibonacci_ sequence
- studying a few _sorting_ algorithms

Throughout the examples and exercices, some algorithmic techniques will be
introduced, the algorithms will be visualized and their complexities will be
studied and measured.

:gear: Throughout this class, you can run the `make test` command to make sure
your implementations are correct. Right now, you should have 11 failing tests,
which is normal and will be fixed as you progress.

You can also be selective and start by running only the required tests, leaving the optional parts untested with
```shell
pytest -k required
```

## The basics with the _Fibonacci_ sequence [[toc](#table-of-content)]
The _Fibonacci_ sequence is defined recursively as follows:
- $F_0 = 0$
- $F_1 = 1$
- $\forall n \geq 2, \quad F_n = F_{n - 1} + F_{n - 2}$

where $F_n$ denotes the $n$-th _Fibonacci_ number.

> :bulb: **Note**
>
> The _Fibonacci_ sequence defined above is one of many more sequences defined
> recursively.
>
> It can be generalized to the following _linear recursive_ series of _degree_
> $m$:
> - $\forall 0 \leq i \lt m, F_i = f_i$
> - $\forall n \geq m, F_m = \sum\limits_{i = 1}^{m} \alpha_i F_{n - i}$
>
> thus, with this generalization, the _Fibonacci_ sequence has a _degree_ of $2$
> and:
> - $f_0 = 0$
> - $f_1 = 1$
> - $\alpha_1 = \alpha_2 = 1$
>
> **Bonus questions**: in the rest of this section, feel free to generalize the
> _Fibonacci_ sequence to any such _linear recursive_ series :wink:

### Q1. [[toc](#table-of-content)]
:file_folder: Create a new file `src/fibo.py`.

:pencil: Using the definition above, implement the _Fibonacci_ sequence using recursion.
It should have the following signature:
- name `fibo`
- arguments
  - `n`: a positive integer
- return the $n$-th _Fibonacci_ number

### Q2. [[toc](#table-of-content)]
:pencil: Add a _main_ block to `src/fibo.py` and compute the $30$ first _Fibonacci_
numbers.

:question: What happens when $n$ grows?
Can you explain why this phenomenon happens by drawing the _call tree_ of $F_n$?

> :bulb: **Hint**
>
> by _call tree_, especially for recursive functions, is here the tree of all
> the recursive calls of a particular function call.
>
> For instance, because computing $F_2$ requires two recursive calls to $F_1$
> and $F_0$ and because the recursion stops at $F_1$ and $F_0$ as they have
> constant values, i.e. they are the base case of the recursive algorithm, the
> _call tree_ of $F_2$ would be:
> ```mermaid
> flowchart TD
>     2(("f(2)"))
>     1(("f(1)"))
>     0(("f(0)"))
>     2 --> 1
>     2 --> 0
> ```

:point_right: The biggest issue of the _recursive_ implementation above is that
the algorithm will recompute values of $F_n$ that have been previously computed.
And because the size of the _computation tree_ explodes exponentially with $n$,
there are exponentially more and more values to compute!!

#### Memoization [[toc](#table-of-content)]
It would be great to reuse the values previously computed, right?...

:exclamation: Enters a powerful _algorithmic_ technique called _memoization_.

The idea is quite straightforward:
- store known intermediate values of the algorithm in a data structure
- if a value has already been computed, then use it
- otherwise, compute the value and store in in the data structure

### Q3. [[toc](#table-of-content)]
:pencil: Write a new `fibo_memo` _recursive_ function in `src/fibo.py` that uses
_memoization_. This function must have one and only one mandatory argument, `n`, but can have other ones that are optionals if needed.

> :bulb: **Note**
>
> in Python, memoization can be achieved using a dictionary.

:question: Can you now compute values of $F_n$ for bigger values of $n$? What is the
maximum $n$ you can feed to `fibo_memo`?

:point_right: You might notice that this recursive version became quite a lot
more complex to read and understand...

And we don't like _hard to understand_!

#### Making algorithms iterative [[toc](#table-of-content)]
:exclamation: Enters another small _algorithmic_ technique that is simply trying
to rewrite an algorithm in its _iterative_ version. That is, instead of calling
itself, the algorithm will simply use a loop to compute the intermediate values.

The _Fibonacci_ algorithm works very nicely with that technique!

### Q4. [[toc](#table-of-content)]
:pencil: Write a last version of the _Fibonacci_ numbers, called `fibo_iter` in
`src/fibo.py` that does not rely on _recursion_ at all.

:question: Is that version still as fast as `fibo_memo`?

---
---
> :point_right: [NEXT](complexity.md)
