from typing import Optional, List

from dataclasses import dataclass

import sys
import pygame


@dataclass
class Canvas:
    width: int
    height: int
    caption: str = ""
    frame_rate: int = 30
    screen: pygame.surface.Surface = None
    clock: pygame.time.Clock = None

    def setup(self):
        pygame.init()

        self.screen = pygame.display.set_mode((self.width, self.height))
        pygame.display.set_caption(self.caption)

        self.clock = pygame.time.Clock()

    def show_list(self, u: List[int], curr: Optional[int]):
        h = self.screen.get_height() / len(u)
        w = self.screen.get_width() / len(u)

        for i, x in enumerate(u):
            if i == curr:
                color = "yellow"
            elif i == x:
                color = "green"
            else:
                color = "red"

            pygame.draw.rect(
                self.screen,
                pygame.color.THECOLORS[color],
                (
                    i * w,
                    self.screen.get_height() - (x + 1) * h,
                    w,
                    (x + 1) * h,
                ),
            )

    def step(self, u: List[int], i: Optional[int]):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pygame.quit()
                    sys.exit()

        if i is not None:
            self.screen.fill((0, 0, 0))
            self.show_list(u, i)
            pygame.display.flip()

        self.clock.tick(self.frame_rate)

    def loop(self, u: List[int]):
        while True:
            self.step(u, None)
