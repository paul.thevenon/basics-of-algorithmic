## An example how to use the `src.ui` module

### The sorting algorithm and its callback
First, we need to write our _sorting algorithm_. It should of course take a list
as its first parameter. In addition, in order to visualize the _sorting_
process, a _callback_ function needs to be passed as well.

Such a _callback_ function should be a function that takes a list of items and
an integer index:
- the list is the full list of values, at a given step of the _sorting_ process
- the integer is the index of the element currently _manipulated_ by the
  algorithm (playing with that index will be interesting to tweak the
  visualization)

So the signature of this function will be `Callable[[List[int], int], None]`.

The _callback_ can be a simple `print` for debugging in the terminal or a more
complex graphical function.

:warning: You are encouraged to use the `step` method from the `src.ui.Canvas`
class.

This is probably how i would write my _sorting_ algorithm:
```python
from typing import Callable, List, Optional


def my_sort(
    u: List[int],
    callback: Optional[Callable[[List[int], int], None]] = None,
) -> List[int]:
    ...

    if callback is not None:
        callback(u, i)

    ...
```

### Setting up the visualization
```python
from ui import Canvas

n = 20
w = 10
h = 5
fps = 30

canvas = Canvas(w * n, h * (n + 1), frame_rate=fps)
canvas.setup()
```

### Create and sort the list
```python
from random import shuffle

# the list of random integers to sort
to_sort_l = list(range(n))
shuffle(to_sort_l)

# sort the list with [`my_sort`] and use `Canvas.step` as the _callback_
sorted_l = my_sort(
    to_sort_l,
    callback=lambda u, i: canvas.step(u, i),
)

# keep the window alive at the end
canvas.loop(sorted_l)
```
