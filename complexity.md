## Studying the complexity of our algorithms [[toc](README.md#table-of-content)]

The fact that the same algorithm can have wildly different perfomances begs the
question: _Can we know the time it will take an algorithm to perform a task
before even running it?_

The answer to this question is obviously "_yes_" and this is what we will see
during this section.

In the next few sections you will learn about
- the different types of complexities
- the distinction between _number of opearations_ and _asymptotic_ complexities
- the $O$, $\Theta$ and $\Omega$ notations for _asymptotic_ complexities
- some classic families of algorithms
- how to compute complexities in practice

### The different types of complexities [[toc](README.md#table-of-content)]
Without defining exactly what _complexities_ really are, we can already feel two
families that we will be interested in.

Because we write algorithms that operates on concrete data structures stored in
memory and that these operations will take real time to complete, we will be
interested in the two following families of _complexities_:
- _space_ complexities: the memory used during the execution of an algorithm
- _time_ complexities: the time it takes to execute a given algorithm to
  completion

These complexities are typically expressed in terms of the parameters of the
algorithm and the size of the input problem, $n$, e.g. the size of a list or the
number of nodes in a graph.

The _space_ complexity will count the number of _memory units_, function of $n$.

The _time_ complexity will count the number of _atomic operation_, function of
$n$.

### _Number of operations_ vs _asymptotic_ complexities [[toc](README.md#table-of-content)]

Before going any further, there is a really important note to be made here!

When talking about complexities, we can refer to two quite different things:
- the _exact number_ of atomic operations or memory used
- the trend when $n$ goes to infinity

The latter is called _asymptotic_ complexity.

:exclamation: an algorithm that has a better _asymptotic_ complexity might be
worse, or even a lot worse, that another algorithm with a bad _asymptotic_
complexity when $n$ stay quite small.

Imagine an algorithm that, in order to become faster for bigger values of $n$,
needs to pay a constant price at the start. It is not hard to feel that another
algorithm which does not pay this price could be a lot better for small values
of $n$.

---
---
> :point_right: [NEXT](complexity_notations.md)
