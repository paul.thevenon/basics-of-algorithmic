## Sorting algorithms [[toc](README.md#table-of-content)]

A lot of times, we gather data in an unpredictable order and we would often like
to have it in a predefined order.

This is the precise goal of _sorting_: given an _order relation_, i.e. something
that tells if some element $a$ is smaller or greater than some other element
$b$, a _sorting_ algorithm will take a list of elements and produce a new list
of elements $[a_1, ... a_n]$ where $[a_1, ... a_n]$ is a permutation of the
original elements and

$$\forall i, j \in [|1, n|], \quad i \leq j \implies a_i \leq a_j$$

For now, we will focus on lists of integers, i.e. where the _order relation_ is
the usual comparison.

In this section, you will learn
- some of the most classic sorting algorithms
- study their complexities
- visualize the algorithms in a fun way
- generalize these algorithms to any type

> to see how to visualize your sorting algorithms, please have a look at
> [this document](src/README.md)

## _Bubble_ sort [[toc](README.md#table-of-content)]
_Bubble_ sort is one of the simplest _sorting_ algorithms and the one you'll be
implementing first!

The idea is to start from the left of the list and _bubble up_ the largest
elements to the top, i.e. if, when comparing two adjacent elements, the left one
is larger than the right one, _bubble_ sort will swap them and move its _cursor_
once to the right.
When the algorithm hits the end of the list, it goes back to the beginning and
starts _bubbling up_ elements again until the list is sorted.

![](./assets/sort/bubble.mp4)

### Q8. [[toc](README.md#table-of-content)]
:pencil: Implement the bubble sort algorithm in a function `bubble_sort` in the
`src/sort.py` module.

:question: What is the complexity of the bubble sort algorithm?
Write your findings in a comment just above the function.

:bulb: Explain your reasoning and answers by following [_How to compute complexities in practice_](complexity_families_and_practice.md#how-to-compute-complexities-in-practice-toc).

:question: Is there anything interesting to say about the _shape of the sorting curve_?

## _Selection_ sort [[toc](README.md#table-of-content)]
_Selection_ sort is quite similar to [_Bubble_ sort](#bubble-sort-toc) but
reduces the number of operations quite a lot.

Once again, the same procedure will be repeated over and over again until the
list is sorted.

_Selection_ sort will iterate over the list and locate the smallest element and
swap it with the first element of the list.
Now that the first element is sorted, the algorithm finds the smallest element
in the rest of the list and swaps it with the second element.
And so on...

![](./assets/sort/selection.mp4)

### Q9. [[toc](README.md#table-of-content)]
:pencil: Implement the selection sort algorithm in a function `selection_sort` in the
`src/sort.py` module.

:question: What is the complexity of the selection sort algorithm?
Write your findings in a comment just above the function.

:bulb: Explain your reasoning and answers by following [_How to compute complexities in practice_](complexity_families_and_practice.md#how-to-compute-complexities-in-practice-toc).

:question: How does it compare to the complexity of the _bubble sort_ algorithm from above?

## _Quick_ sort [[toc](README.md#table-of-content)]
_Quick_ sort will be our first _Divide and Conquer_ sorting algorithm.

The idea is to repeat the following procedure until the array is sorted:
- choose a pivot element, e.g. the first one, the last one or a random one
- move all smaller elements to its left and all larger elements to its right
- the pivot is now sorted
- apply the quicksort algorithm on the left and right parts recursively

![](./assets/sort/quick.mp4)

### Q10. [[toc](README.md#table-of-content)]
:pencil: Implement the quick sort algorithm in a function `quick_sort` in the
`src/sort.py` module.

:question: Do you understand better how the algorithm works with the visualization?

## _Merge_ sort [[toc](README.md#table-of-content)] (_optional_)
The _Merge_ sort algorithm is yet another example of a _Divide and Conquer_
algorithm.

The gist of it is as follows:
- split the list in two parts of equal length, the left and the right of the
  list
- apply _Merge_ sort recursively on these two parts
- once the two parts have been sorted, merge them together

![](./assets/sort/merge.mp4)

### Q11. [[toc](README.md#table-of-content)]
:pencil: Implement the merge sort algorithm in a function `merge_sort` in the
`src/sort.py` module.

:question: What is the complexity of the merge sort algorithm?
Write your findings in a comment just above the function.

:bulb: Explain your reasoning and answers by following [_How to compute complexities in practice_](complexity_families_and_practice.md#how-to-compute-complexities-in-practice-toc).

> :bulb: **Hint**
>
> you can use the [_The Master Theorem_](complexity_families_and_practice.md#the-master-theorem-toc)
> to compute this complexity...

:question: Do you understand better how the algorithm works with the visualization?

## Q12. [[toc](README.md#table-of-content)]
:file_folder: Create a new file called `src/sorting_time.py`.

:pencil: Measure the time it takes to sort lists with your sorting algorithms to confirm
that your implementations follow the theoretical complexities indeed. You can plot the results with
`matplotlib`.

> :bulb: **Note**
>
> You can measure for lists of size $2^n$ for $n$ reasonable as _bubble sort_ is very very slow
> on big lists...

## Generalizing to any type [[toc](README.md#table-of-content)]

In this section, you will generalize your sorting algorithms to any kind of
input data.

So far, we have been assuming the items in the lists are similar to "numbers",
i.e. we can apply to them an _order relation_ similar to $\lt$, e.g. we can say
$1 \lt 2$ with the order on integers or $"bar" \lt "foo"$ with the
_lexicographic_ order on strings of characters.

However, with this approach, we cannot sort a table by the values stored in a
given column!

In Python, comparing two `dict`s gives a `TypeError`
```python
alice = {"name": "Alice", "age": 23}
bob = {"name": "Bob", "age": 21}

alice < bob
```
```
TypeError: '<' not supported between instances of 'dict' and 'dict'
```

### Q13. [[toc](README.md#table-of-content)]
:file_folder: Create a new file called `src/generic_sort.py`.

You can start by copy pasting your sorting algorithms from `sort.py` into this
new module, no need to rewrite them from scratch :wink:

:pencil: Add a new argument to your sorting algorithms. It should be a function taking
two items $a$ and $b$ from the input list to sort and return
- a strictly negative number if $a \lt b$
- a strictly positive number if $b \lt a$
- $0$ if $a = b$

This new argument should be optional and have a default value that will sort
numbers only in ascending order.

### Q14. [[toc](README.md#table-of-content)]
:pencil: In a _main_ Python block, make sure your algorithms still work on simple lists
as before.

> :bulb: **Note**
>
> you should have tests to make sure everything works fine, but feel free to
> add the examples you want!

Try to sort an integer list in reverse.

Try to sort a string list following _lexicographic_ ordering.

Try to sort the CSV table stored in `data/mk8_wr.csv`
- what is the name of the first track by name in _lexicographic_ order?
- what is the name of the player who's got the record in the fastest track?
- what is the name of the track which has been a world record for the longest?

> :bulb: **Hint**
>
> you can use the `$.Track`, `$.Time`, `$.Player` and `$.Duration` columns

:gear: Running `python src/generic_sort.py` should print the answers to the 5 questions above.

## Visualization. [[toc](README.md#table-of-content)]

### Q15. [[toc](README.md#table-of-content)]

:mag: Read the first section of the [_`src.ui` documentation_](src/README.md)

:pencil: Add this "_visualization callback_" argument to your sorting algorithms from `src/generic_sort.py`.

Your sorting functions must have the following parameters, *in this order*:
- `arr`: the list to sort
- `cmp`: a _callback_ function that compares 2 elements of the list (_optional_, defaults to `None`)
- `viz`: a _callback_ function to visualize the execution (_optional_, defaults to `None`)

> :bulb: **Note**
>
> the names of your arguments don't matter, only their order

This `viz` _callback_ function, as stated in the [documentation](src/README.md) takes 2 parameters:
- the `list` you are sorting
- the `index` of the item you are comparing/manipulating, which will be shown with a different color
  in the visualization.

### Q16. [[toc](README.md#table-of-content)]

:file_folder: Create a new file called `src/visual_sort.py`.

:mag: Read the rest of the [_`src.ui` documentation_](src/README.md)

:pencil: Write the code to visualize your bubble sort implementation. Make sure the visualization
window is small enough to fit into a single screen and that it runs in a _reasonable_ amount of
time, e.g. less than 5 seconds, by choosing an appropriate size for the input array.

:gear: Running `python src/visual_sort.py` should show the algorithm in action.

---
---
> this is the end of the class :clap: :clap:
>
> [return to TOC](README.md#table-of-content)
